<?php
# 	HSBXL REST API - SpaceAPI endpoint
#	(c) 2021 Frederic Pasteleurs <frederic@askarel.be>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


# redirect to main page
if (!isset($GLOBALCONFIGFILE))
{
    header('Status: 301 Moved Permanently', false, 301);
    header('Location: ../index.php');
    exit;
}

function spaceapi_dump($Parameters) {
    // Use Pamela to determine if the space is open
    $PamData = json_decode (file_get_contents('https://pam.hsbxl.be/macs.php', false, stream_context_create(array('ssl' => array('verify_peer' => false, 'verify_peer_name' => false)))));
    foreach ($PamData as &$PamItem) $pammachines[] = array ('mac' => $PamItem, 'name' => $PamItem);
    $pamela_decode = array_diff($PamData, array('gate'));
    if (sizeof($pamela_decode) == 0) {
	    $SpaceOpen = false;
	} else {
	    $SpaceOpen = true;
	}

    $SpaceAPI= array (
	'api' => '0.13',
	'api_compatibility' => array ('14'),
	'space' => $GLOBALS['GLOBALCONFIG']['CORE']['orgname'],
	'logo' => $GLOBALS['GLOBALCONFIG']['SPACEAPI']['logo'],
	'url' => $GLOBALS['GLOBALCONFIG']['SPACEAPI']['url'],
	'spacefed' => $GLOBALS['GLOBALCONFIG']['SPACEAPI']['spacefed'],
	'location' => $GLOBALS['GLOBALCONFIG']['SPACEAPI']['location'],
	'contact' => $GLOBALS['GLOBALCONFIG']['SPACEAPI']['contact'],
	'issue_report_channels' => $GLOBALS['GLOBALCONFIG']['SPACEAPI']['issue_report_channels'],
	'state' => array (
	    'open' => $SpaceOpen,
	    ),
	'sensors' => array (
	    'network_connections' => array (
		array (
		    'type' => 'wired',
		    'name' => 'Pamela-legacy',
		    'location' => 'main site',
		    'description' => 'Using ye olde Pamela scanner',
		    'value' => count($PamData),
		    'machines' => $pammachines,
		),
		array (
		    'type' => 'wired',
		    'name' => 'Example-wired-network',
		    'location' => 'main site',
		    'description' => 'wired-network for main site',
		    'value' => 2,
		    'machines' => array (
			array ('name' => 'lorem', 'mac' => '00:12:34:56:78:ab'),
			array ('name' => 'ipsum', 'mac' => '00:12:34:ab:cd:ef')
		    )
		),
		array (
		    'type' => 'wifi',
		    'name' => 'Example-wifi-network',
		    'location' => 'main site',
		    'description' => 'wifi-network for main site',
		    'value' => 3,
		    'machines' => array (
			array ('name' => 'foo', 'mac' => '00:67:89:56:78:ab'),
			array ('name' => 'bar', 'mac' => '00:34:21:ab:cd:ef')
			)
		    )
		)
	    )
    );
    output_array_as_json($SpaceAPI);
}

function sensor_create($Parameter) {
    output_array_as_json(array($Parameter, 'sensor_create'));
    
}

function sensor_update($Parameter) {
    output_array_as_json(array ($Parameter, "sensor_update"));
    
}
function sensor_delete($Parameter) {
    output_array_as_json(array ($Parameter, "sensor_delete"));
    
}
function sensor_list($Parameter) {
    output_array_as_json(array ($Parameter, "sensor_list"));
    
}

register_api_call ('get', '/spaceapi', 'spaceapi_dump', null, 'SpaceAPI public request');

# https://www.restapitutorial.com/lessons/httpmethods.html
register_api_call ('post', '/spaceapi-sensor-manage', 'sensor_create', 'SIMPLEHTTP', 'Create a sensor endpoint');
register_api_call ('delete', '/spaceapi-sensor-manage', 'sensor_delete', 'SIMPLEHTTP', 'Delete a sensor endpoint');
register_api_call ('get', '/spaceapi-sensor-manage', 'sensor_list', null, 'List defined sensors');
register_api_call ('patch', '/spaceapi-sensor-update', 'sensor_update', null, 'Update a sensor endpoint. Use sensor key for authentication');
register_api_call ('put', '/spaceapi-sensor-update', 'sensor_update', null, 'Update a sensor endpoint. Use sensor key for authentication');

?>
