<?php
# 	HSBXL REST API - Barcode management 
#	(c) 2021 Frederic Pasteleurs <frederic@askarel.be>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


# redirect to main page
if (!isset($GLOBALCONFIGFILE))
{
    header('Status: 301 Moved Permanently', false, 301);
    header('Location: ../index.php');
    exit;
}

$BARCODES=array(
    "" => array('description' => 'Default barcode namespace: EAN codes', 'barcodes' => array (
	"SPACE:SHIRT" => 0,
	"" => "",
	"" => "",
	"" => "",
	"" => "",
	"" => "",
	"" => "",
	"" => "")),
    "I" => array('description' => 'Inventory namespace', 'barcodes' => array (
	"" => "",
	"" => "",
	"" => "",
	"" => "",
	"" => "",
	"" => "",
	"" => "",
	"" => ""))
);

#echo "barcode";
#yaml_emit ($PRODUCTS);
#var_dump($PRODUCTS);

#output_array_as_json($PRODUCTS);



?>