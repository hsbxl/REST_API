<?php
# 	HSBXL REST API - Product management / Zoppas legacy DB generator
#	(c) 2021 Frederic Pasteleurs <frederic@askarel.be>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# redirect to main page
if (!isset($GLOBALCONFIGFILE))
{
    header('Status: 301 Moved Permanently', false, 301);
    header('Location: ../index.php');
    exit;
}

#$PRODUCTFILE=$GLOBALS['MYLOCATION'] . '/data/products.json';


function dumpProducts($parameters) {
#    $LOADEDPRODUCTS=json_decode (file_get_contents($GLOBALS['PRODUCTFILE']), true);
    $LOADEDPRODUCTS=json_decode (file_get_contents($GLOBALS['GLOBALCONFIG']['PRODUCTS']['PRODUCTFILE']), true);
    if ($LOADEDPRODUCTS==null) output_array_as_json (array ('error' => 'JSON decode failed', 'message' => 'Failed to decode file "' . $GLOBALS['GLOBALCONFIG']['PRODUCTS']['PRODUCTFILE'] . '"'), 500 );

//    var_dump($LOADEDPRODUCTS, $parameters);
    output_array_as_json($LOADEDPRODUCTS);
}

// Format the loaded array as an old style CSV
function dumpLegacyCSV($parameters) {
#    $LOADEDPRODUCTS=json_decode (file_get_contents($GLOBALS['PRODUCTFILE']), true);
    $LOADEDPRODUCTS=json_decode (file_get_contents($GLOBALS['GLOBALCONFIG']['PRODUCTS']['PRODUCTFILE']), true);
    if ($LOADEDPRODUCTS==null) output_array_as_json (array ('error' => 'JSON decode failed', 'message' => 'Failed to decode file "' . $GLOBALS['GLOBALCONFIG']['PRODUCTS']['PRODUCTFILE'] . '"'), 500 );
    foreach ($LOADEDPRODUCTS as $prodkey => $prodvalue) {
	if ($prodvalue['legacydisplay']) printf ("%s;%s;%s;;;%s;%s;;;\n", $prodvalue['legacyCode'], $prodvalue['description'], number_format ($prodvalue['sellingPrice'][''],2,'.',''),  number_format ($prodvalue['sellingPrice'][''],2,'.',''),  number_format ($prodvalue['sellingPrice']['bytenight'], 2, '.',''));
    }
    exit(0);
}

register_api_call ('GET', '/products/dumpProducts', 'dumpProducts', false, 'test function to dump product list');
register_api_call ('GET', '/products/zoppaslegacy', 'dumpLegacyCSV', false, 'Dump JSON as legacy CSV for old Zoppas');

?>