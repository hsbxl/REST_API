This is the HSBXL REST API.

All space functions will be progressively moved and centralized to this API.

Functionalities to move:
- Pamela
- Password resetter
- Shared password app
- Old concierge.sh script with following functions:
   User management (create/modify/adopt/revoke)
   Bank management (import/export/list/stat)
   LDAP export
   mass-mailer

The following functionalities are already present and working, even if pretty crude:
- SpaceAPI
- Zoppas price list

This API has hard requirement on LDAP and MySQL

The application is divided in two sections
- The server side, to run on a PHP-enabled server somewhere on the internet
- The client side, which can be run on basically anything connected to the internet

Open question:
- Authentication method is still up in the air: Some API calls will need authentication. 
  The least we can do is simple authentication, but it is impractical and not very secure.
  