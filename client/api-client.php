#!/usr/bin/env php
<?php
# 	HSBXL REST API - Commandline client
#	(c) 2021 Frederic Pasteleurs <frederic@askarel.be>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# User configuration directory
$APP['USERCONFIGDIR']=$GLOBALS['_SERVER']['HOME'] . '/.config/' . $GLOBALS['_SERVER']['SCRIPT_FILENAME'];
# History file location
$APP['HistFile']=$APP['USERCONFIGDIR'] . '/history';
# Private key location (public key can be extracted from it)
$APP['PrivateKeyFile']=$APP['USERCONFIGDIR'] . '/privatekey.pem';
# Config file location
$APP['JSONconfig']=$APP['USERCONFIGDIR'] . '/config.json';		// The New
# Default username
$APP['remote_username']='anonymous';

# Test existence of config directory. Create it if it doesn't
if (!is_dir($APP['USERCONFIGDIR'])) 
    if (!mkdir ($APP['USERCONFIGDIR'], 0700, true)) {
	printf ("Cannot create directory %s: exit.\n", $APP['USERCONFIGDIR']);
	exit(1);
    }

# Load configuration from JSON file.Builds the JSON from obsolete PHP snippet if absent. Will need rework.
if (file_exists($APP['JSONconfig'])) { 
    $GLOBALCONFIG=json_decode (file_get_contents ($APP['JSONconfig']), true);
    } else { // Builds the JSON from old PHP config
    if (!file_exists(dirname (__FILE__) . '/clientconfig.php')) {
	printf ( "File %s does not exist. Please run the setup script to create config file.\n", dirname (__FILE__) . '/clientconfig.php');
	exit(1);
    }
    include_once (dirname (__FILE__) . '/clientconfig.php');
    file_put_contents ($APP['JSONconfig'], json_encode ($GLOBALCONFIG));
    printf ("JSON config created!\n");
    exit (0);
    }

# Create private key if it does not exist. Load it if it exists
if (!file_exists($APP['PrivateKeyFile'])) {
	printf ("Generating new keypair... ");
	$pkey = openssl_pkey_new();
	openssl_pkey_export ($pkey, $APP['PrivateKey']);
	printf ("Saving to file... ");
	file_put_contents ($APP['PrivateKeyFile'],$APP['PrivateKey']);
	printf ("Done.\n");
    } else {
	$APP['PrivateKey'] = file_get_contents ($APP['PrivateKeyFile']);
	$pkey=openssl_pkey_get_private($APP['PrivateKey']);
    }

$opensslkey = openssl_pkey_get_details($pkey);


# Bring the command history
if (file_exists($APP['HistFile'])) readline_read_history ($APP['HistFile']);

while (true){
    $APP['CommandLine'] = readline (sprintf ("[%s@%s]> ", $APP['remote_username'], $GLOBALCONFIG['CORE']['api_url']));
    if ($APP['CommandLine']!= '') readline_add_history ($APP['CommandLine']);
    $APP['exploded_cmdline'] = explode (' ', $APP['CommandLine']);
    switch ($APP['exploded_cmdline'][0]) {
/// This space for rent
	// Bank operations
	case 'bank':
	    switch ($APP['exploded_cmdline'][1]) {

		case '':
		    break;
		default:
		    printf ("Unknown bank command: %s\n", $APP['exploded_cmdline'][1]);
	    }
	    break;


/// This space for rent
	case 'auth':
	    $APP['password']='';
	    printf ("Username: "); $APP['attempted_username'] = trim(fgets(STDIN));
	    $char='';
	    printf ("Password: "); system('stty -echo'); $APP['attempted_password'] = trim(fgets(STDIN)); system('stty echo'); printf ("\n");
		printf ("Entered Password:'%s'\n", $APP['attempted_password']);
	    break;
	case 'dumpglobals':
	    var_dump ($GLOBALS);
	    break;
	case 'exit':
	    printf ("Bye\n");
	    readline_write_history ($APP['HistFile']);
	    exit (0);
	    break;
	case 'help':
	    printf ("Known commands: %s\n", 'help, exit, dumpglobals, auth, bank');
	    break;
	case '': // Empty lines means nothing.
	    break;
	default:
	    printf ("Unknown command: %s\n", $APP['exploded_cmdline'][0]);
    }
}
#	call_user_func($sysscreen);
?>